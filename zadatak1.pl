#!/usr/bin/perl

print "Upisite niz znakova: ";
chomp($niz = <STDIN>);
if (!$niz) {
    die "Niz znakova treba biti neprazan\n";
}
print "Upisite broj ponavljanja: ";
chomp($brPon = <STDIN>);
if ($brPon < 1) {
    die "Broj ponavljanja mora biti pozitivan broj\n";
}
foreach (1..$brPon) {
    print "$niz\n";
}