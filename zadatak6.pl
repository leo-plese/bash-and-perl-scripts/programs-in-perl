#!/usr/bin/perl

use open ':locale';

if (@ARGV < 1) {
    die "Treba bar 1 arg (duljina prefiksa)\n";
}

$prefixLen = $ARGV[$#ARGV];
if ($prefixLen < 1) {
    die "Duljina prefiksa treba biti min 1\n";
}
@ARGV = @ARGV[0..($#ARGV-1)];
$brArg = @ARGV;

@canOpenFiles = ();
foreach (@ARGV) {
    if (!(open DAT, "<", $_)) {
        print "Ne mogu otvoriti datoteku $_.\n";
        next;
    }
    push @canOpenFiles, $_;
}
@ARGV = @canOpenFiles;
if (!@ARGV && $brArg) {
    die "\n";
}
#print "ARGVaft = @ARGV\n";
#print "brarg = $brArg\n";

#print "ARGVaft" . @ARGV . "\n";

#print "\n------\n";
$curFile=$ARGV[0];
if ( $brArg) {
    print "\nDatoteka: $curFile\n";
    print "---------------\n";
}
$flag = 0;
%prefFreq = ();
while (<>) {
    if (eof(ARGV) && $brArg) {
        $flag=1;
        
        if ($_ ne "\n") {
            &countPrefs;
        }
        next;
    }
    if ($flag) {
        $curFile = $ARGV;
        &printWords;
        %prefFreq = ();
        
        print "\nDatoteka: $curFile\n";
        print "---------------\n";
        
        $flag = 0;
    }

    chomp;
    
    &countPrefs;
}

&printWords;


sub countPrefs {
    #print "Procitao: $_\n";
    s/(\W)+/ /g;
    #print ">$_<\n";
    @lineWords = split;
    #$"="X";
    #print "lineWords = >@lineWords<\n";
    #print "lineWords" . ">" . @lineWords . "<\n";


    @prefs = ();
    foreach $word (@lineWords) {
        $curPref = "";
        @chs = split "", $word;
        #print "chs = @chs\n";
        foreach $ic (1..$prefixLen) {
            $curPref .= $chs[$ic-1];
        }
        if (length($curPref) == $prefixLen) {
            push @prefs, $curPref;
        }
    }
    
    foreach $pref (@prefs) {
        #print "prefBef = <$pref>\n";
        $pref = lc($pref);
        #print "prefAft = <$pref>\n";
        $prefFreq{$pref}++;
    }
}


sub printWords {
    {
        #use locale;
        use Unicode::Collate::Locale;
        my $Collator = new Unicode::Collate::Locale::
                     locale => "hr",
                     level  => 1,
                     normalization => undef,
                    ;
        @keysSorted =$Collator->sort(keys %prefFreq);
        #@keysSorted = sort keys %prefFreq;
    }
    foreach $key (@keysSorted) {
        print "$key : $prefFreq{$key}\n";
    }

    #while (($k, $v) = each %prefFreq) {
    #    print "$k => $v\n";
    #}
}