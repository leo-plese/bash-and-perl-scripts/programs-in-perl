#!/usr/bin/perl

$brArg = @ARGV;

if ($brArg > 1) {
    die "Treba dati ime najvise 1 datoteke za citanje.\n";
}

if (!(open DAT, "<", $ARGV[0])) {
    die "Ne mogu otvoriti datoteku $ARGV[0]\n";
}

sub printProblem {
    #print "$field[0] $field[1] $field[2] - PROBLEM: $startDate $startHMS --> $endTime\n";
    push @problems, "$field[0] $field[1] $field[2] - PROBLEM: $startDate $startHMS --> $endTime";
}

@problems = ();
while (<>) {
    # prvi redak datoteke/unosa preko STDIN je "JMBAG;Prezime;Ime;Termin;Zakljucano" pa njega preskacem
    if ($lineNum == 0) {
        $lineNum++;
        next;
    }
    chomp;
    #print "redak$lineNum= $_ \n";
    @field = split /;/;
    #$" = "X";
    #print "field = @field\n";
    $startTime = $field[3];
    @startTime = split /\s+/, $startTime;
    $startDate = $startTime[0];
    $startHMS = $startTime[1];
    $endTime = $field[4];
    @endTime = split /\s+/, $endTime;
    $endDate = $endTime[0];
    $endHMS = $endTime[1];
    #print "@startTime : $endTime\n";
    #print "$startDate, $startHMS : $endTime\n";
    #print "$startDate, $startHMS : $endDate, $endHMS\n";
    
    @startDateField = split /-/, $startDate;
    @endDateField = split /-/, $endDate;
    $startY = $startDateField[0];
    $endY = $endDateField[0];
    $startM = $startDateField[1];
    $endM = $endDateField[1];
    $startD = $startDateField[2];
    $endD = $endDateField[2];
    
    @startTimeSec = split /:/, $startHMS;
    @endTimeSec = split /:/, $endHMS;
    $startTimeSec = $startTimeSec[0]*3600 + $startTimeSec[1]*60;
    $endTimeSec = $endTimeSec[0]*3600 + $endTimeSec[1]*60 + $endTimeSec[2];

    
    $isStartYDiv4 = ($startY % 4 == 0);
    
    if ($endY < $startY) {
        &printProblem;
        #print "PROBLEM 1\n";
        next;
    } elsif ($endY == $startY) {
        if ($endM < $startM) {
            &printProblem;
            #print "PROBLEM 2\n";
            next;
        } elsif ($endM == $startM) {
            if ($endD < $startD) {
                &printProblem;
                #print "PROBLEM 3\n";
                next;
            } elsif ($endD == $startD) {
                if (!($endTimeSec >= $startTimeSec && $endTimeSec <= $startTimeSec+3600)) {
                    &printProblem;
                    #print "PROBLEM 4\n";
                    next;
                }
            } else {
                if ($endD > $startD+1) {
                    &printProblem;
                    #print "PROBLEM 5\n";
                    next;
                }
                if (((86400 - $startTimeSec) + $endTimeSec) > 3600) {
                    &printProblem;
                    #print "PROBLEM 6\n";
                    next;
                }
            }
        } else {
            if ($endM > $startM+1) {
                &printProblem;
                #print "PROBLEM 7\n";
                next;
            }
            
            if ($endD != 1) {
                &printProblem;
                #print "PROBLEM 8\n";
                next;
            }
            
            if (!(($startM == 1 && $startD == 31) ||
                    ($startM == 2 && (($startD == 29 && $isStartYDiv4) || ($startD == 28 && !$isStartYDiv4))) ||
                    ($startM == 3 && $startD == 31) ||
                    ($startM == 4 && $startD == 30) ||
                    ($startM == 5 && $startD == 31) ||
                    ($startM == 6 && $startD == 30) ||
                    ($startM == 7 && $startD == 31) ||
                    ($startM == 8 && $startD == 31) ||
                    ($startM == 9 && $startD == 30) ||
                    ($startM == 10 && $startD == 31) ||
                    ($startM == 11 && $startD == 30) ||
                    ($startM == 12 && $startD == 31))) {
                &printProblem;
                #print "PROBLEM 9\n";
                next;
            }
            
            if (((86400 - $startTimeSec) + $endTimeSec) > 3600) {
                &printProblem;
                #print "PROBLEM 10\n";
                next;
            }
            
        }
    } else {
        if ($endY > $startY+1) {
            &printProblem;
            #print "PROBLEM 11\n";
            next;
        }
        
        if (!($endM == 1 && $endD == 1)) {
            &printProblem;
            #print "PROBLEM 12\n";
            next;
        }
        
        if (!($startM == 12 && $startD == 31)) {
            &printProblem;
            #print "PROBLEM 13\n";
            next;
        }
        
        if (((86400 - $startTimeSec) + $endTimeSec) > 3600) {
            &printProblem;
            #print "PROBLEM 14\n";
            next;
        }
    }
    
    
    
    $lineNum++;
}
$"="\n";
print "\n@problems\n";

