#!/usr/bin/perl

$brArg = @ARGV;

if ($brArg != 1) {
    die "Treba dati ime tocno 1 datoteke za citanje.\n";
}

if (! open STUDFILE, "<", $ARGV[0]) {
    die "Ne mogu otvoriti datoteku $ARGV[0]\n";
}

chomp($line = <STUDFILE>);
#print "LINE=$line\n";
@bodKomp = split /;/, $line;
#print "bodKomp=@bodKomp\n";
$numKomp = @bodKomp;
#print "numKomp=$numKomp\n";

while (<>) {
    if ($lineNum == 0) {
        $lineNum++;
        next;
    }
    chomp;
    #print "red=$_\n";
    
    #$"="X";
    @field = split /;/;
    #print "field=@field\n";
    $k = join ";", @field[0..2];
    #print "k=$k\n";
    
    $v = join ";", @field[3..(2+$numKomp)];
    #print "v=$v\n";
    
    #$v =~ s/\b-\b/0/g;
    $v =~ s/^-$/0/g;
    
    $studRez{$k} = $v;
    
    $lineNum++;
}

#print "\n-----\n";
#while (($key, $value) = each %studRez) {
#    print "$key => $value\n";
#}

#print "\n-----\n";
while (($key, $value) = each %studRez) {
    @value = split /;/, $value;
    @izracunateKomp = ();
    foreach (0..$numKomp) {
        push @izracunateKomp, $bodKomp[$_] * $value[$_];
    }
    $sumBod = 0;
    #print "$key - izracunateKomp = @izracunateKomp\n";
    foreach (@izracunateKomp) {
       $sumBod += $_;
    }
    $studBod{$key} = $sumBod;
}

#print "\n-----\n";
#while (($key, $value) = each %studBod) {
#    print "$key => $value\n";
#}

#print "\n-----\n";
@keysSorted = sort { $studBod{$b} <=> $studBod{$a} } keys %studBod;
#print "keysSort = @keysSorted\n";
#print "\n-----\n";
print "Lista po rangu:\n";
print "-------------------\n";
$i = 0;
@chCntArr = ();
@studDataArr = ();
foreach (@keysSorted) {
    @studDataField = split /;/;

    $studData = $studDataField[1] . ", " . $studDataField[2] . " (" . $studDataField[0] .")";
    @studDataArr[$i] = $studData;
    
    #print "<$studData>\n";
    @chs = split("", $studData);

    $chCnt = 0;
    $specChs = 0;
    foreach $d (@chs) {
        #print "d=$d\n";
        if (!((ord($d) >= 48 && ord($d) <= 57) || (ord($d) >= 65 && ord($d) <= 90) || (ord($d) >= 97 && ord($d) <= 122) || ord($d) == 44 || ord($d) == 32 || ord($d) == 40 || ord($d) == 41)) {
            $specChs++;
            #print "0\n";
            next;
        }
        #print "1\n";
        $chCnt++;
    }
    $trueChCnt = $chCnt+ $specChs/2; 
    $chCntArr[$i] = $trueChCnt;
    
    $i++;
}

$mel = &maxElem;
#print "mel = $mel\n";
#print "studData = @studDataArr\n";
#print "\n**********\n";
$i = 1;
foreach (@keysSorted) {
    $printStr = "";
    $studDataArr[$i-1] = $studDataArr[$i-1] . (" " x ($mel - $chCntArr[$i-1]));
    printf "%3d. %-10s : %.2f\n", $i, $studDataArr[$i-1],$studBod{$_};
    $i++;
}

sub maxElem {
    my $curmax;
    $curmax = 0;
    foreach (@chCntArr) {
        if ($_ > $curmax) {
            $curmax = $_;
        }
    }
    return $curmax;
}
