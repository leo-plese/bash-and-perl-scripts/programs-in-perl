#!/usr/bin/perl

sub printFreqDict {
    my($hour, $trueHour);
    
    foreach $hour (sort keys %dayFreq) {
        while ($hour > $trueHour) {
            #print "  $trueHour : 0\n";
            printf "  %02d : 0\n", $trueHour;
            $trueHour++;
        }
        #print "  $hour : $dayFreq{$hour}\n";
        printf "  %02d : %d\n", $hour, $dayFreq{$hour};
        $trueHour++;
    }
    if ($trueHour < 23) {
        while ($trueHour < 24) {
            #print "  $trueHour : 0\n";
            printf "  %02d : 0\n", $trueHour;
            $trueHour++;
        }
    }

}

$brArg = @ARGV;
@canOpenFiles = ();
foreach (@ARGV) {
    if (!(open DAT, "<", $_)) {
        print "Ne mogu otvoriti datoteku $_.\n";
        next;
    }
    push @canOpenFiles, $_;
}
@ARGV = @canOpenFiles;
if (!@ARGV && $brArg) {
    die "\n";
}

#print "<@ARGV>\n";
$brArg = @ARGV;
$curFile=$ARGV[0];
@curFileData = split /\./, $curFile;
$curDate = $curFileData[-2];
if ($brArg) {
    print " Datum: $curDate\n";
    print " sat : broj pristupa\n";
    print "-------------------------------\n";
}

%dayFreq = ();


$flag = 0;
while (<>) {
    if (eof(ARGV) && $brArg) {
        $flag=1;
        
        if ($_ ne "\n") {
            &incForHour;
        }
        next;
    }
    
    #if ($_ eq "\n") {
    if (/^\n$/) {
        next;
    }
    
    if ($flag) {
        $curFile = $ARGV;
        &printFreqDict;
        print "\n";
        %dayFreq = ();
        @curFileData = split /\./, $curFile;
        $curDate = $curFileData[-2];
        print " Datum: $curDate\n";
        print " sat : broj pristupa\n";
        print "-------------------------------\n";
        
        $flag = 0;
    }
    
    
    chomp;

    &incForHour;
    
}

if (! $brArg) {
    print " sat : broj pristupa\n";
    print "-------------------------------\n";
} 

&printFreqDict;


sub incForHour {
    @lineData = split /:/;
    $hour = $lineData[1];
    $dayFreq{$hour} += 1;
    #print "h=$hour\n";
    
    #print "Procitao: $_\n";
}